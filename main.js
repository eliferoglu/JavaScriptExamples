var input1 = $("#sayi1");
var input2 = $("#sayi2");

var topla = $("#topla");
var cikar = $("#cikar");
var carp = $("#carp");
var bol = $("#bol");
var temizle = $("#temizle");

var sonuc_html = $("#sonuc");

var gecmis = $("#gecmis");
var gecmis_array = [];

function click_olayi(event) {
  var operator = event.target.innerHTML;
  if(input1.val() == "" || input2.val() == "") {
    sonuc_html.html("lütfen sayı giriniz")
  } else {
    hesapla(operator);
    listele();
  }
}

function listele() {
  gecmis.html("");
  gecmis_array.forEach(function(eleman) {
    gecmis.append("<li>" + eleman.deger1 + eleman.islem + eleman.deger2 + "=" + eleman.sonuc + "</li>")
  });
}

function hesapla(islem) {
  var sayi1 = parseInt(input1.val());
  var sayi2 = parseInt(input2.val());
  var sonuc;
  switch (islem) {
    case "+":
      sonuc = sayi1 + sayi2;
      break;
    case "-":
      sonuc = sayi1 - sayi2;
      break;
    case "x":
      sonuc = sayi1 * sayi2;
      break;
    case "/":
      if(sayi2 == 0) {
        sonuc = "tanımsız"
      } else {
        sonuc = sayi1 / sayi2;
      }
      break;
  }
  var obj = {
    deger1: input1.val(),
    deger2: input2.val(),
    islem: islem,
    sonuc: sonuc
  };
  gecmis_array.push(obj);
  sonuc_html.html("<strong>" + sonuc + "</strong>")
}

topla.on("click", click_olayi);
cikar.on("click", click_olayi);
carp.on("click", click_olayi);
bol.on("click", click_olayi);

temizle.on("click", function() {
  input1.val("");
  input2.val("");
  sonuc_html.html("");
});

